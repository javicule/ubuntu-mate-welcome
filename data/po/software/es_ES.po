#
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"Report-Msgid-Bugs-To: you@example.com\n"
"POT-Creation-Date: 2016-03-18 10:38+0100\n"
"PO-Revision-Date: 2016-03-18 10:38+0100\n"
"Last-Translator: \n"
"Language-Team: : LANGUAGE\n"
"Language: es_ES\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7.1\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: software.html:17
msgid "Software"
msgstr "Software"

#: software.html:17
msgid "Boutique"
msgstr "Boutique"

#: software.html:46
msgid "Software Boutique"
msgstr "Boutique de Software"

#: software.html:47
msgid ""
"There is an abundance of software available for Ubuntu MATE and some people "
"find that choice overwhelming. The Boutique is a carefully curated selection "
"of the best-in-class applications chosen because they integrate well, "
"complement Ubuntu MATE and enable you to self style your computing "
"experience."
msgstr ""

#: software.html:54
msgid "If you can't find what you're looking for, install one of the"
msgstr "Si no puedes encontrar lo que estás buscando, instala uno los"

#: software.html:56
msgid "software centers"
msgstr "centros de software"

#: software.html:57
msgid "to explore the complete Ubuntu software catalog."
msgstr "para explorar el completo catálogo de software para Ubuntu."

#: software.html:61
msgid "More Apps Await."
msgstr "Más Apps te esperan."

#: software.html:62
msgid ""
"Connect your computer to the Internet to explore a wide selection of "
"applications for Ubuntu MATE."
msgstr ""
"Conecta tu ordenador a Internet para explorar la amplia selección de "
"aplicaciones para Ubuntu MATE."

#: software.html:64
msgid ""
"Once online, you'll be able to download and install tried & tested software "
"right here in Welcome. Our picks ensure that the software featured "
"integrates well the Ubuntu MATE desktop."
msgstr ""

#: software.html:70
msgid ""
"Sorry, Welcome could not establish a connection to the Ubuntu repository "
"server. Please check your connection and try again."
msgstr ""
"Lo siento, Bienvenido no pudo establecer conexión con el servidor de "
"repositorios de Ubuntu. Por favor, comprueba tu conexión e inténtalo de "
"nuevo."

#: software.html:75
msgid "Retry Connection"
msgstr "Reintentar Conexión"

#: software.html:79
msgid "Connection Help"
msgstr "Ayuda con la Conexión"

#: software.html:85
msgid "Get Connected."
msgstr "Conéctate."

#: software.html:86
msgid ""
"Here's a few things you can check, depending on the type of connection you "
"have:"
msgstr ""
"Aquí hay algunas cosas que puedes comprobar, dependiendo del tipo de "
"conexión que tengas:"

#: software.html:87
msgid "Wired Connection"
msgstr "Conexión Cableada"

#: software.html:89
msgid "Is the cable securely plugged in?"
msgstr "¿Está el cable conectado con seguridad?"

#: software.html:90
msgid ""
"Is the router online and can other devices access the network? Try "
"restarting the router."
msgstr ""
"¿Está el router en línea y pueden otros dispositivos acceder a la red? "
"Intenta reiniciar el router."

#: software.html:93
msgid "Wireless Connection"
msgstr "Conexión Inalámbrica"

#: software.html:94
msgid "Have you entered the correct wireless password from the"
msgstr ""
"¿Has introducido la contraseña correcta de la red inalámbrica desde el "

#: software.html:94
msgid "network indicator"
msgstr "indicador de red"

#: software.html:94
msgid "in the upper-right?"
msgstr "arriba a la derecha?"

#: software.html:96
msgid "When disconnected, the applet looks like this:"
msgstr "Al estar desconectado, el applet se ve de esta manera:"

#: software.html:98
msgid ""
"Is there a physical switch that may have accidentally switched off wireless "
"functionality?"
msgstr ""
"¿Hay un interruptor físico que pueda haber apagado accidentalmente la "
"funcionalidad inalámbrica?"

#: software.html:100
msgid "Is there a soft (keyboard) switch that toggles it off?"
msgstr "¿Hay un interruptor por teclado que la apague?"

#: software.html:102
msgid "You may need to hold the FN key while pressing the function key."
msgstr ""
"Puede que necesitas mantener pulsado la tecla FN mientras presionas la tecla "
"de función."

#: software.html:103
msgid "Here's an example:"
msgstr "Aquí tienes un ejemplo:"

#: software.html:107
msgid "Not working at all or experiencing sluggish connections?"
msgstr "¿No funciona en absoluto o estás experimentando conexiones lentas?"

#: software.html:108
msgid ""
"Sorry to hear that. You will need a temporary wired connection to install "
"working drivers."
msgstr ""
"Siento oír eso. Necesitarás una conexión cableada temporal para instalar "
"controladores que funcionen."

#: software.html:110
msgid "The"
msgstr "La"

#: software.html:110
msgid "Additional Drivers"
msgstr "pestaña de Controladores Adicionales"

#: software.html:110
msgid "tab may have them available for your system."
msgstr "puede que los tenga disponibles para tu sistema."

#: software.html:111
msgid ""
"Otherwise, you will need to manually install third party drivers for your "
"hardware, or a download specific package containing the firmware. See"
msgstr ""
"Si no, necesitarás instalar manualmente controladores de terceros para tu "
"hardware, o descargar un paquete específico que contenga el firmware. Ver"

#: software.html:112
msgid "Drivers"
msgstr "Controladores"

#: software.html:112
msgid "in the"
msgstr "en la"

#: software.html:112
msgid "Getting Started"
msgstr "sección Primeros Pasos"

#: software.html:113
msgid "section for more details."
msgstr "para más detalles."

#: software.html:114
msgid "Feel free to"
msgstr "Siéntete libre de"

#: software.html:114
msgid "ask the community"
msgstr "preguntar a la comunidad"

#: software.html:114
msgid "if you need assistance."
msgstr "si necesitas ayuda."

#: software.html:119
msgid "OK"
msgstr "VALE"

#: software.html:131
msgid "Accessories"
msgstr "Accesorios"

#: software.html133, 146, 159, 172, 185, 198, 211, 224, 237
msgid "No Filter"
msgstr "Sin Filtro"

#: software.html:135
msgid "Handy utilities for your computing needs."
msgstr "Herramientas prácticas para tus necesidades informáticas."

#: software.html:144
msgid "Education"
msgstr "Educación"

#: software.html:148
msgid "For study and children."
msgstr "Para estudiar y para niños."

#: software.html:157
msgid "Games"
msgstr "Juegos"

#: software.html:161
msgid "A selection of 2D and 3D games for your enjoyment."
msgstr "Una selección de juegos 2D y 3D para tu disfrute."

#: software.html:170
msgid "Graphics"
msgstr "Gráficos"

#: software.html:174
msgid "For producing and editing works of art."
msgstr "Para producir y editar obras de arte."

#: software.html:183
msgid "Internet"
msgstr "Internet"

#: software.html:187
msgid "For staying connected and enjoying the features of your own cloud."
msgstr ""
"Para mantenerse conectado y disfrutar de las características de tu propia "
"nube."

#: software.html:196
msgid "Office"
msgstr "Oficina"

#: software.html:200
msgid "For more then just documents and spreadsheets."
msgstr "Para algo más que sólo documentos y hojas de cálculo."

#: software.html:209
msgid "Programming"
msgstr "Desarrollo"

#: software.html:213
msgid "For the developers and system administrators out there."
msgstr "Para los desarrolladores y administradores de sistema por ahí fuera."

#: software.html:222
msgid "Sound & Video"
msgstr "Sonido y Vídeo"

#: software.html:226
msgid "Multimedia software for listening and production."
msgstr "Software multimedia para escuchar y producir."

#: software.html:235
msgid "System Tools"
msgstr "Herramientas de Sistema."

#: software.html:239
msgid "Software that makes the most out of your system resources."
msgstr "Software que aprovecha al máximo los recursos de tu sistema."

#: software.html:248
msgid "Universal Access"
msgstr "Acceso Universal"

#: software.html:249
msgid "Software that makes your computer more accessible."
msgstr "Software que hace a tu ordenador más accesible."

#: software.html:258
msgid "Servers"
msgstr "Servidores"

#: software.html:259
msgid "One-click installations for serving the network."
msgstr "Instalaciones de Un-click prestar servicios en red."

#: software.html:268
msgid "Discover More Software"
msgstr "Descubre Más Software"

#: software.html:269
msgid ""
"Graphical interfaces to browse a wide selection of software available for "
"your operating system."
msgstr ""
"Interfaces Gráficas para explorar una amplia selección de software "
"disponible para tu sistema operativo."

#: software.html:277
msgid "Miscellaneous Fixes"
msgstr "Reparaciones Diversas"

#: software.html:278
msgid ""
"This section contains operations that can fix common problems should you "
"encounter an error while upgrading or installing new software."
msgstr ""
"Esta sección contiene operaciones que pueden arreglar problemas comunes como "
"encontrar un error al actualizar o instalar nuevo software."

#: software.html:285
msgid "Outdated Package Lists?"
msgstr "¿Listas de Paquetes Obsoletas?"

#: software.html:286
msgid "Your repository lists may be out of date, which can cause"
msgstr ""
"Tus listas de repositorios pueden estar obsoletas, lo que puede ocasionar"

#: software.html:286
msgid "Not Found"
msgstr "errores de No Encontrado"

#: software.html:286
msgid "errors and"
msgstr "e"

#: software.html:286
msgid "outdated version"
msgstr "información desactualizada"

#: software.html:287
msgid ""
"information when trying to install new or newer versions of software. This "
"is particularly the case when Ubuntu MATE connects online for the first time "
"after installation."
msgstr ""
"al intentar instalar versiones nuevas o más recientes del software. Este es "
"particularmente el caso cuando Ubuntu MATE se conecta en línea por primera "
"vez después de la instalación."

#: software.html:290
msgid "Update the repository lists:"
msgstr "Actualiza las listas de repositorios:"

#: software.html:294
msgid "Update Sources List"
msgstr "Actualizar Lista de Fuentes"

#: software.html:295
msgid "Upgrade Installed Packages"
msgstr "Actualizar Paquetes Instalados"

#: software.html:299
msgid "Broken Packages?"
msgstr "¿Paquetes Rotos?"

#: software.html:300
msgid "When a previous installation or removal was interrupted"
msgstr "Cuando una instalación o eliminación previa fue interrumpida"

#: software.html:301
msgid "(for instance, due to power failure or loss of Internet connection)"
msgstr ""
"(por ejemplo, debido a un fallo de corriente o una pérdida de conexión a "
"Internet)"

#: software.html:302
msgid ""
"further software cannot be added or removed without properly re-configuring "
"these broken packages. If necessary, you may need to also resolve "
"dependencies and install any missing packages in order for the software to "
"run properly."
msgstr ""
"no puede ser añadido o eliminado más software sin reconfigurar "
"apropiadamente estos paquetes rotos. Si es necesario, puede que necesites "
"solucionar dependencias e instalar cualesquiera paquetes ausentes con el fin "
"de ejecutar el software correctamente."

#: software.html:308
msgid "Configure packages that were unpacked but not configured:"
msgstr "Configurar paquetes que fueron desempaquetados pero no configurados:"

#: software.html:312
msgid "Download and install broken dependencies:"
msgstr "Descargar e instalar dependencias rotas:"

#: software.html:316
msgid "Configure Interrupted Packages"
msgstr "Configure Paquetes Interrumpidos"

#: software.html:317
msgid "Resolve Broken Packages"
msgstr "Solucionar Paquetes Rotos"

#: software.html:321
msgid ""
"In addition, listed below are terminal equivalent commands that otherwise "
"appear in the"
msgstr ""
"Además, listados abajo tienes comandos equivalentes de la terminal que de "
"todas formas aparecen en el"

#: software.html:322
msgid "Software Updater"
msgstr "Actualizador de Software"

#: software.html:324
msgid "Upgrade all packages that have a new version available:"
msgstr "Actualizar todos los paquetes que tengan una nueva versión disponible:"

#: software.html:328
msgid "Upgrade to a new release of Ubuntu MATE:"
msgstr "Actualizar a una nueva versión de Ubuntu MATE:"

#: software.html:342
msgid "Filter by Software License"
msgstr "Filtrar por Licencia de Software"

#: software.html:343
msgid "Hide Proprietary Software"
msgstr "Ocultar Software Propietario"

#: software.html:345
msgid "Show Terminal Commands"
msgstr "Mostrar Comandos de Terminal"

#: software.html:351
msgid "Proprietary Software"
msgstr "El Software Propietario"

#: software.html:352
msgid ""
"is owned by an individual or company. There may be restrictions on its usage "
"(like a license agreement), and provides no access to the source code."
msgstr ""

#: software.html:355
msgid "Free and Open Source Software"
msgstr "El Software Libre y de Código Abierto"

#: software.html:356
msgid ""
"grants the user the freedom to share, study and modify the software without "
"restriction. For instance, the GPL is an example of a 'Free Software' "
"license."
msgstr ""
"concede al usuario la libertad de compartir, estudiar y modificar el "
"software sin restricción. Por ejemplo, la GPL es un caso de una licencia de "
"\"Software Libre\""

#~ msgid ""
#~ "There is an abundance of software available for Ubuntu MATE and some "
#~ "people find that choice overwhelming. This is carefully curated selection "
#~ "of best-in-class applications have been chosen because they integrate "
#~ "well, complement Ubuntu MATE and enable you to self style your computing "
#~ "experience."
#~ msgstr ""
#~ "Hay software en abundancia disponible para Ubuntu MATE y algunas personas "
#~ "encuentran esa elección abrumadora. Esta cuidadosamente revisada "
#~ "selección de las mejores aplicaciones han sido escogidas porque se "
#~ "integran bien, complementan Ubuntu MATE y te habilitan para diseñar tu "
#~ "propia experiencia informática."

#~ msgid ""
#~ "Once online, you'll be able to download and install that have been tried "
#~ "& tested for your desktop right here in Welcome. Our picks ensure that "
#~ "this software integrates well the Ubuntu MATE desktop."
#~ msgstr ""
#~ "Una vez en línea, serás capaz de descargar e instalar aplicaciones que "
#~ "han sido probadas y testeadas para tu escritorio justo aquí en "
#~ "Bienvenido. Nuestras elecciones garantizan que este software se integra "
#~ "bien con el escritorio de Ubuntu MATE."

#~ msgid ""
#~ "is owned by an individual or company. There may be restrictions on its "
#~ "usage (like a license agreement), and no access to its source code."
#~ msgstr ""
#~ "es propiedad de un individuo o compañía. Puede haber restricciones en "
#~ "cuanto a su uso (como un acuerdo de licencia), y no hay acceso a su "
#~ "código fuente."
